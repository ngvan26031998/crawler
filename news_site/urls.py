
from django.urls import path

from . import views

urlpatterns = [
    path('', views.manage_crawler, name='manage-crawler'),
    path('show_data/', views.show_data, name='show-data'),
    path('politic/', views.show_politic, name='show-politic'),
    path('business/', views.show_business, name='show-business'),
    path('technology/', views.show_technology, name='show-technology'),
    path('sports/', views.show_sports, name='show-sports'),
    path('world/', views.show_world, name='show-world'),
    path('science/', views.show_science, name='show-science'),
    path('health/', views.show_health, name='show-health'),
    path('life/', views.show_life, name='show-life'),
    path('opinion/', views.show_opinion, name='show-opinion'),
    path('clear_documents/', views.clear_documents, name = 'clear-documents'),
    path('update_realime_site/', views.update_realime_site, name = 'update-realime-site'),
    #path('crawl/', views.crawl, name='crawl'),
    #path('schedule_crawling/', views.schedule_crawling, name='schedule_crawling'),
    #path('remove_job/', views.remove_job, name='remove_job'),
    #path('update_scrawling/', views.update_scrawling, name='update_scrawling'),
    #path('stop_crawl_site/', views.stop_crawl_site, name='stop_crawl_site'),
    path('select_site_master/', views.select_site_master, name='select_site_master')
]

