import datetime
import json
from sty import fg, bg, ef, rs, RgbFg


def init_state(site_id):
    with open('news_site/resource/state_management.json', 'r+') as f:
        data = json.load(f)
        data[str(site_id)] = {
            'total': -1,
            'current': -1,
            'state': 'init'
        }
        f.seek(0)
        json.dump(data, f, indent=4)
        f.truncate()


def get_state(site_id):
    try:
        with open('news_site/resource/state_management.json', 'r+') as f:
            return json.load(f)[str(site_id)]
    except:
        return None


def write_state(site_id=None, total=None, current=None, state=None):
    try:
        with open('news_site/resource/state_management.json', 'r+') as f:
            data = json.load(f)
            if total:
                data[str(site_id)]['total'] = total
            if current:
                data[str(site_id)]['current'] = current
            if state:
                data[str(site_id)]['state'] = state
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()
    except Exception as e:
        print(fg.red + 'write state error : ' + str(e) + fg.rs)


def read_state():
    try:
        with open('news_site/resource/state_management.json', 'r+') as f:
            return json.load(f)
    except:
        return {}


def add_stop(site_id):
    if site_id:
        with open('news_site/resource/stop_management.json', 'r+') as f:
            data = json.load(f)
            data['stop_list'][str(site_id)] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()


def remove_stop(site_id):
    with open('news_site/resource/stop_management.json', 'r+') as f:
        stop_data = json.load(f)
        if str(site_id) in stop_data['stop_list']:
            stop_data['stop_list'].pop(str(site_id))
            f.seek(0)
            json.dump(stop_data, f, indent=4)
            f.truncate()


def check_stop(site_id):
    with open('news_site/resource/stop_management.json', 'r+') as f:
        stop_data = json.load(f)
        if str(site_id) in stop_data['stop_list']:
            return True
        else:
            return False
