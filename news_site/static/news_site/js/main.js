console.log('Hello')

function getDataOnSiteMaster() {
    d = document.getElementById("site_master").value;
    console.log('get data')
    $.ajax({
      //url: "{% url 'select_site_master' %}",
      url: "select_site_master/",
      data: { "site_master_id": d},
      dataType:"json",
      type: "get",
      success: function(data){
        document.getElementById("content").innerHTML = '';
        var tbody = document.getElementById("content")
        for(var i=0; i < data['result'].length;i++){
          var row = document.createElement("tr")
          var col1 = document.createElement("td")
          col1.setAttribute('style', 'text-align:center')
          col1.textContent = data['result'][i]['name']
          var col2 = document.createElement("td")
          col2.textContent = data['result'][i]['last_succ_count']
          col2.setAttribute('style', 'text-align:center')
          var col3 = document.createElement("td")
          col3.textContent = data['result'][i]['total_crawling']
          col3.setAttribute('style', 'text-align:center')
          col3.setAttribute('id', 'total_crawling_' + String(data['result'][i]['id']))
          col3.setAttribute('class', 'total_crawling')

          var col4 = document.createElement("td")
          col4.setAttribute('id', 'state_' + String(data['result'][i]['id']))
          if(data['result'][i]['state'] == '1'){
            div = document.createElement("div")
            div.setAttribute('class', 'lds-dual-ring')
            col4.append(div)
          }else{
            div = document.createElement("div")
            div.setAttribute('class', 'check')
            col4.append(div)
            console.log('here')
          }


          var col5 = document.createElement("td")
          col5.setAttribute('style', 'text-align:center')
          col5.textContent = 'Schedule will run at ' + data['result'][i]['schedule_hour'] + '/60' 
          var col6 = document.createElement("td")
          var button = document.createElement('button');
          button.innerHTML = 'Clear'
          button.setAttribute('id', data['result'][i]['id'])
          button.addEventListener('click', function(){
            console.log('Delete All documents')
            site_id = this.id
            $.ajax({
              //url: "{% url 'clear-documents' %}",
              url:"clear_documents/",
              data: { "site_id": site_id},
              dataType:"json",
              type: "get",
              success: function(data){
                td_total_crawling = document.getElementById('total_crawling_' + data['site_id'])
                td_total_crawling.textContent = '0'
                td_tongsobai = $('#tongsobai').text()//document.getElementById('tongsobai').value
                td_conlai = parseInt(td_tongsobai) - parseInt(data['sobaixoa'])
                document.getElementById('tongsobai').innerHTML = td_conlai

              }
            })
          });
          col6.append(button)

          row.append(col1)
          row.append(col2)
          row.append(col3)
          row.append(col4)
          row.append(col5)
          row.append(col6)
          tbody.append(row)
        }
        var row = document.createElement("tr")
        var col1 = document.createElement("td")
        col1.textContent = 'TỔNG SỐ BÀI'
        col1.setAttribute('style', 'text-align:center')
        var col2 = document.createElement("td")
        col2.textContent = ''
        col2.setAttribute('style', 'text-align:center')
        var col3 = document.createElement("td")
        col3.textContent = data['tongsobai']
        col3.setAttribute('style', 'text-align:center')
        col3.setAttribute('id', 'tongsobai')
        var col4 = document.createElement("td")
        var col5= document.createElement("td")


        row.append(col1)
        row.append(col2)
        row.append(col3)
        row.append(col4)
        row.append(col5)
        tbody.append(row)

        var row1 = document.createElement("tr")
        var col1 = document.createElement("td")
        col1.textContent = 'TỔNG SỐ SITE CON'
        col1.setAttribute('style', 'text-align:center')
        var col2 = document.createElement("td")
        col2.textContent = ''
        col2.setAttribute('style', 'text-align:center')
        var col3 = document.createElement("td")
        col3.textContent = data['numb_sites']
        col3.setAttribute('style', 'text-align:center')
        col3.setAttribute('id', 'numb_sites')
        var col4 = document.createElement("td")
        var col5= document.createElement("td")


        row1.append(col1)
        row1.append(col2)
        row1.append(col3)
        row1.append(col4)
        row1.append(col5)
        tbody.append(row1)
      }

    });
  }

  function clear_document(site_id){
    console.log('Delete All documents')
    console.log(site_id)
    $.ajax({
      //url: "{% url 'clear-documents' %}",
      url: "clear_documents/",
      data: { "site_id": site_id},
      dataType:"json",
      type: "get",
      success: function(data){
        td_total_crawling = document.getElementById('total_crawling_' + data['site_id'])
        td_total_crawling.textContent = '0'
        td_tongsobai = $('#tongsobai').text()//document.getElementById('tongsobai')
        //console.log(parseInt(td_tongsobai))
        td_conlai = parseInt(td_tongsobai) - parseInt(data['sobaixoa'])
        document.getElementById('tongsobai').innerHTML = td_conlai

      }
    })
  }

  // prev_click = ''
  // function link_highlight(clicked_id){
  //   if(prev_click != '')
  //   {
  //     $('#' + prev_click).removeClass('active').addClass('inactive');
  //   }
  //   console.log(clicked_id)
  //   $('#' + clicked_id).addClass('active');
  //   prev_click = clicked_id
  // }

  function update_crawling(message, person) {
    d = document.getElementById("site_master")
    if(d){
      d = document.getElementById("site_master").value;
      $.ajax({

        url: "update_realime_site/",
        data: { "site_master_id": d},
        traditional : true,
        dataType:"json",
        type: "get",
        success: function(data){
  
          for(var i=0; i < data['result'].length;i++){
            document.getElementById("state_" + data['result'][i]['id']).innerHTML = '';
            col_statusCrawl = document.getElementById("state_" + data['result'][i]['id'])
            if(data['result'][i]['state'] == '1'){
              div = document.createElement("div")
              div.setAttribute('class', 'lds-dual-ring')
              col_statusCrawl.append(div)
            }else{
              div = document.createElement("div")
              div.setAttribute('class', 'check')
              col_statusCrawl.append(div)
            }
            id = 'total_crawling_' + String(data['result'][i]['id'])
  
            document.getElementById(id).innerHTML = data['result'][i]['total_crawling']
          }
          document.getElementById('tongsobai').innerHTML = data['tongsobai']
    
          
       }
      
      })
    }
   
  }
  setInterval(update_crawling, 2000);
