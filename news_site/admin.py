from django.contrib import admin
from news_site.models import Categories, Languages, Sites, Documents, SiteAttributes, DownloadWarning,SiteMaster,ReadyCrawledDocument
# Register your models here.
admin.site.register(Categories)
admin.site.register(Languages)
admin.site.register(Sites)
admin.site.register(Documents)
admin.site.register(SiteAttributes)
admin.site.register(DownloadWarning)
admin.site.register(SiteMaster)
admin.site.register(ReadyCrawledDocument)
