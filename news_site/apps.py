from django.apps import AppConfig
import sys


class NewsSiteConfig(AppConfig):
    name = 'news_site'

    def ready(self):
        pass
