#https://www.crummy.com/software/BeautifulSoup/bs4/doc/
from bs4 import BeautifulSoup
import re
#from datetime import datetime, timedelta, date, time
from datetime import datetime as dt, timedelta as td
import datetime
import pytz
from django.utils import timezone

NOISE_JP = "In a time of both"
NOISE_GBT = ["&#160;", "&#13;", "Global Times"]
NOISE_FOX = ["Fox News Flash top headlines for", "Fox News Flash top headlines",
             "CLICK HERE TO GET THE FOX NEWS APP", "GET THE FOX NEWS APP", 
             "Click for more from", 
             "CLICK HERE FOR THE FOX NEWS APP", 
             "The Associated Press",
             "Click to view original post.",
             "CLICK HERE TO GET THE OPINION NEWSLETTER"]
			 #CLICK HERE TO READ MORE FROM DAN GAINOR
NOISE_CNBC = ["Subscribe to CNBC on YouTube."]
NOISE_REUTERS = ["Our Standards: The Thomson Reuters Trust Principles."]

def del_script(response):
	del_script = re.sub('<script.*?>', '', str(response), flags=re.DOTALL)
	del_script = re.sub('</script.*?>', '', del_script, flags=re.DOTALL)
	return del_script

#BỎ CÁC THẺ HTML
def striphtml(data, tag_type = 'all'):
	data = data.replace('\\n', '')
	data = data.replace('\\t', '')
	data = data.replace("\\'", "'")

	data = data.replace("&amp;", "&")
	data = data.replace("won&#8217;t", "won't")
	data = data.replace("&#8217;", "'")
	data = data.replace("&#8216;", "'")
	data = data.replace("&#226;&#128;&#153;", "'")
	data = data.replace("&#165;", "¥")#japantimes
	data = data.replace("&#160;", " ")#globaltimes
	data = data.replace("&#8211;", " - ")#cnbc
	data = data.replace("&#8212;", " - ")#cnbc
	
	remove_img = re.sub('<img.*?>', ' ', data, flags=re.DOTALL)
	remove_a = re.sub('<a.*?>', ' ', remove_img, flags=re.DOTALL)
	p = re.compile(r'<.*?>')
	data_remove_tag = p.sub(' ', remove_a)

	if tag_type == 'tags':
		data_remove_tag = data_remove_tag.replace('Updated', '')
	elif tag_type == 'author':
		data_remove_tag = data_remove_tag.replace('By', '')
		data_remove_tag = data_remove_tag.replace('| Fox News', '')

	return data_remove_tag.strip()

def remove_special_character(data):
	newString = re.sub(r'\t', '', data)
	newString = re.sub(r'\n', '', newString)
	newString = re.sub(r'\\n\\t\\t\\t\\t\\t', '', newString)
	newString = re.sub(r'\\t\\t\\t\\t', '', newString)
	newString = re.sub(r'\n\t\t\t', '', newString)
	return newString


convert_month = {
	'Jan' : 1,
	'Feb' : 2,
	'Mar' : 3,
	'Apr' : 4,
	'May' : 5,
	'Jun' : 6,
	'Jul' : 7,
	'Aug' : 8,
	'Sep' : 9,
	'Oct' : 10,
	'Nov' : 11,
	'Dec' : 12,
	'January' : 1,
	'February' : 2,
	'March' : 3,
	'April' : 4,
	'June' : 6,
	'July' : 7,
	'August' : 8,
	'September' : 9,
	'October' : 10,
	'November' : 11,
	'December' : 12	
}

#//div[@class="article-date"]/time
def convert_time_foxnew(string_date):
	remove_html = striphtml(string_date)
	date_result = None
	split_date = remove_html.split()
	if 'ago' in remove_html:
		if (('day' in remove_html) or ('days' in remove_html)):
			d = dt.today() - td(days=int(split_date[0]))
			#date_result = datetime.combine(date(int(d.year), int(d.month), int(d.day)), time(d.hour, d.minute, d.second))
			date_result = datetime.datetime(int(d.year), int(d.month), int(d.day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_result 
		elif (('hour' in remove_html) or ('hours' in remove_html)):
			d = dt.today() - td(hours=int(split_date[0]))
			#date_result = datetime.combine(date(int(d.year), int(d.month), int(d.day)), time(d.hour, d.minute, d.second))
			date_result = datetime.datetime(int(d.year), int(d.month), int(d.day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_result 
		elif (('month' in remove_html) or ('months' in remove_html)):
			d = dt.today() - td(days=int(split_date[0]) * 30)
			#date_result = datetime.combine(date(int(d.year), int(d.month), int(d.day)), time(d.hour, d.minute, d.second))
			date_result = datetime.datetime(int(d.year), int(d.month), int(d.day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_result
		elif (('min' in remove_html) or ('mins' in remove_html)):
			d = dt.today() - td(minutes=int(split_date[0]))
			#date_result = datetime.combine(date(int(d.year), int(d.month), int(d.day)), time(d.hour, d.minute, d.second))
			date_result = datetime.datetime(int(d.year), int(d.month), int(d.day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_result
		return None
	else:
		d = dt.today()
		if len(split_date) == 2:
			month = convert_month[split_date[0]]
			#date_result = datetime.combine(date(int(d.year), int(month), int(date_str_split[1])), time(d.hour, d.minute, d.second))
			date_result = datetime.datetime(int(d.year), month, int(split_date[1]), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_result
		elif len(split_date) == 3:	
			month = convert_month[split_date[0]]
			#date_result = datetime.combine(date(int(date_str_split[2]), int(month), int(date_str_split[1][:-1])), time(d.hour, d.minute, d.second))
			date_result = datetime.datetime(int(split_date[2]), int(month), int(split_date[1][:-1]), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_result
		else:
			return None


def process_datetime(string_date, site_master):
	d = dt.today()
	try:
		if site_master == 'https://www.globaltimes.cn/':
			remove_html = striphtml(string_date)
			split_string = remove_html.split()
			month = convert_month[split_string[1]]
			day = split_string[2][:-1]
			year = split_string[3]
			date_time = datetime.datetime(int(year), int(month), int(day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_time
		#elif site_master == 'https://www.foxnews.com/':
		#	return convert_time_foxnew(string_date)
		elif site_master == 'https://www.cnbc.com/':
			remove_html = striphtml(string_date)
			split_string = remove_html.split()
			month = convert_month[split_string[2]]
			day = split_string[3]
			year = split_string[4]
			date_time = datetime.datetime(int(year), int(month), int(day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_time
		elif site_master == 'https://www.reuters.com/' or site_master == 'https://www.foxnews.com/':
			split_string = string_date.split('-')
			month = split_string[1]
			day = split_string[2][0:2]
			year = split_string[0]
			#date_time = datetime.combine(date(int(year), int(month), int(day)), time(d.hour, d.minute, d.second))
			date_time = datetime.datetime(int(year), int(month), int(day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return date_time

		elif site_master == 'https://www.japantimes.co.jp/':
			remove_html = striphtml(string_date)
			remove_special = remove_special_character(remove_html)
			split_string = remove_special.split()
			month = convert_month[split_string[0]]
			day = split_string[1][:-1]
			year = remove_special_character(split_string[2])
			date_time = datetime.datetime(int(year), int(month), int(day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
			return datetime.datetime(int(year), int(month), int(day), hour=d.hour, minute=d.minute, second=d.second, tzinfo=pytz.UTC)
		return datetime.datetime(10, 10, 10, hour=10, minute=10, second=10, tzinfo=pytz.UTC)
	except:
		return datetime.datetime(10, 10, 10, hour=10, minute=10, second=10, tzinfo=pytz.UTC)

# XỬ LÝ NỘI DUNG CRAWL GỐC
# XÓA CÁC THẺ KHÔNG LIÊN QUAN ĐẾN NỘI DUNG
# PHÂN ĐOẠN CHO BÀI VIẾT
# XỬ LÝ NHIỄU
# BỎ CÁC THẺ HTML
def preprocess(html, site, mode):
	content_mix_tag = []
	contents = []
	contents_not_noise = []
	if site == 'globaltimes':
		content_mix_tag = html.split('<br />')
		if mode == 'display-user':
			for c in content_mix_tag:
				cont_tmp = striphtml(MarkSection(str(c)))
				if cont_tmp != '':
					contents.append(cont_tmp)
		else:
			for c in content_mix_tag:
				contents.append(striphtml(str(c)))

		contents_not_noise = process_noise(contents, site)		
		pass
	else:
		soup = BeautifulSoup(html, 'html.parser')
		for i in range(len(soup.find_all(['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6','li']))):	
			content_mix_tag.append(soup.find_all(['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li'])[i])

		if mode == 'display-user':
			for c in content_mix_tag:
				cont_tmp = striphtml(MarkSection(str(c)))
				if cont_tmp != '':
					contents.append(cont_tmp)
		else:
			for c in content_mix_tag:
				contents.append(striphtml(str(c)))

		contents_not_noise = process_noise(contents, site)
	return contents_not_noise

# BỎ CÁC CÂU XUẤT HIỆN Ở HẦU HẾT CÁC BÀI
# ĐÓ LÀ CÁC LỜI CẢM ƠN, GIỚI THIỆU, HAY CÁC NỘI DUNG KHÔNG LIÊN QUAN ĐẾN BÀI VIẾT
def process_noise(contents, site):
	if site == 'japantimes':
		try:
			del_noise = None
			for i in reversed(range(len(contents))):
				if NOISE_JP in contents[i]:
					del_noise = contents[0:i]
					break
			#contents[-1] = (contents[-1].strip()).replace('-->', '')
			return del_noise[0:-3]
		except:
			return contents
	if site == 'reuters':
		try:
			pref_author = contents[0].split()[0]
			time_line = contents[1].split()
			
			last_post_noise = contents[-1].split()
			contents[-1] = " ".join(last_post_noise)
			contents[-1] = str(contents[-1]).replace(NOISE_REUTERS[0], '')
			if pref_author == 'By' and len(time_line) == 3 and time_line[1] == 'Min' and time_line[2] == 'Read':
				return contents[2:]
			
			return contents
		except:
			return contents
	if site == 'cnbc':
		if NOISE_CNBC[0] in contents[-1]:
			return contents[0:-1]
		return contents
	if site == 'foxnews':
		try:
			if NOISE_FOX[0] in contents[0]:
				contents[0] = ''
			if NOISE_FOX[0] in contents[1]:
				contents[1] = ''
			if NOISE_FOX[1] in contents[0]:
				contents[0] = ''
			if NOISE_FOX[1] in contents[1]:
				contents[1] = ''

			content_res = []
			for j in range(len(contents)):
				for i in range(2, len(NOISE_FOX)):
					if (NOISE_FOX[i] in contents[j]):
						contents[j] = contents[j].replace(NOISE_FOX[i], '')
				if contents[j].strip() != '':
					content_res.append(contents[j])
			if "contributed to this report." in content_res[-1]:
				content_res[-1] = ''
			return content_res
		except:
			return ['None']
	if site == 'globaltimes':
		try:
			for i in range(len(contents)):
				contents[i] = re.sub(NOISE_GBT[0], '', contents[i], flags=re.DOTALL)
				contents[i] = re.sub(NOISE_GBT[1], '', contents[i], flags=re.DOTALL)
			
			if contents[-1].strip() == 'Global Times':
				return contents[1:-1]
			return contents[1:]
		except:
			return ['None']
	if site == '':
		return contents

#ĐÁNH DẤU ĐOẠN HAY CÂU VĂN LÀ TIÊU ĐỀ, CẦN IN ĐẬM TRONG VĂN BẢN
def MarkSection(tag_section):
    new_html =  re.sub('<h1.*?h1>','<p> !#h1#! ' + tag_section + ' !#h1#! </p>',tag_section)
    new_html =  re.sub('<h2.*?h2>','<p> !#h2#! ' + tag_section + ' !#h2#! </p>',new_html)
    new_html =  re.sub('<h3.*?h3>','<p> !#h3#! ' + tag_section + ' !#h3#! </p>',new_html)
    new_html =  re.sub('<h4.*?h4>','<p> !#h4#! ' + tag_section + ' !#h4#! </p>',new_html)
    new_html =  re.sub('<h5.*?h5>','<p> !#h5#! ' + tag_section + ' !#h5#! </p>',new_html)
    new_html =  re.sub('<h6.*?h6>','<p> !#h6#! ' + tag_section + ' !#h6#! </p>',new_html)
    return new_html

def display_document(contents):
	for content in contents:
		print(content)
		print('\n')

