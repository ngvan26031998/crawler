import random

from django.contrib.auth.models import User
from django.db import models

random.seed(2021)


class Languages(models.Model):
    LangCode = models.CharField(max_length=10, unique=True)
    LangName = models.CharField(max_length=100)
    DateFormat = models.CharField(max_length=50, null=True, blank=True)
    ThousandSep = models.CharField(max_length=50, null=True, blank=True)
    DecimalSep = models.CharField(max_length=50, null=True, blank=True)
    IsDefault = models.BooleanField()
    Order = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.LangName

class SiteMaster(models.Model):
    RESPONSE_TYPE = (
        ('HTML', 'HTML'),
        ('JSON', 'JSON')
    )
    TIME_UNIT_PUBLISHED = (
        ('DIRECT', 'DIRECT'),
        ('INDIRECT', 'INDIRECT')
    ) 
    MasterName = models.CharField(max_length=100)
    language = models.ForeignKey(
        Languages, on_delete=models.SET_NULL, null=True, default=1)
    response_type = models.CharField(choices=RESPONSE_TYPE, max_length=100, default='HTML')
    time_unit_published = models.CharField(choices=TIME_UNIT_PUBLISHED, max_length=100, default='DIRECT')
    def __str__(self):
        return self.MasterName

class Categories(models.Model):
    Parent = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    CateName = models.CharField(max_length=500)
    Order = models.IntegerField(null=True, blank=True)

    def __str__(self):
        if self.Parent:
            return self.Parent.CateName + ' -> ' + self.CateName
        else:
            return self.CateName



class Documents(models.Model):
    """
    this class use for storing data after crawling succesfully
    """
    STATUS_CHOICES = (
        (0, 'New'),
        (1, 'Crawling'),
        (2, 'Stop'),
        (3, 'Crawled'),
    )

    LinkDoc = models.CharField(
        max_length=2000, null=True, blank=True, unique=True, db_index=True)#link tài liệu
    Title = models.TextField(null=True, blank=True)#tiêu đề bài
    Abstract = models.TextField(null=True, blank=True)#tóm lược ý chính
    Author = models.TextField(null=True, blank=True)#tác giả
    Content = models.TextField(null=True, blank=True)#nội dung
    DateDoc = models.DateTimeField(null=True, blank=True)#
    date_time_str = models.TextField(null=True, blank=True)
    Tags = models.TextField(null=True, blank=True)#các thẻ loại bài viết liên quan
    Thumbnail = models.ImageField(null=True, blank=True)#
    JsonDoc = models.TextField(null=True, blank=True)#
    DownloadTime = models.IntegerField(default=0)#
    RefreshState = models.IntegerField(choices=STATUS_CHOICES, default=0)
    ViewCount = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.Title

class Sites(models.Model):
    """
    in this class , you can get the news xpath and next page of this site
    """
    STATUS_CHOICES = (
        (0, 'New'),
        (1, 'Crawling'),
        (2, 'Stop'),
        (3, 'Crawling_Finished'),
        (4, 'Initial')
    )
    SOURCE_TYPE = (
        ('NEWS', 'NEWS'),
        ('STORYBOOK', 'STORYBOOK'),
        ('ADMINISTRATIVE_DOCUMENT,', 'ADMINISTRATIVE_DOCUMENT'),
        ('LEGISLATION,', 'LEGISLATION'),
        ('SOCIAL_MEDIA-COMMENT', 'SOCIAL_MEDIA-COMMENT'),
        ('SOCIAL_MEDIA-BLOG', 'SOCIAL_MEDIA-BLOG'),
        ('SOCIAL_MEDIA-FACEBOOK', 'SOCIAL_MEDIA-FACEBOOK'),
    )

    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, default=1)
    category = models.ForeignKey(
        Categories, on_delete=models.SET_NULL, null=True)
    master_name = models.ForeignKey(
        SiteMaster, on_delete=models.SET_NULL, null=True)
    documents =  models.ManyToManyField('Documents', through = 'Sites_Documents')

    SiteName = models.CharField(max_length=500)
    SiteLink = models.CharField(max_length=500, unique=True)
    source_type = models.CharField(
        choices=SOURCE_TYPE, max_length=100, default='NEWS')
    State = models.IntegerField(choices=STATUS_CHOICES, default=0)
    last_download_count = models.IntegerField(default=0)
    last_succ_count = models.IntegerField(default=0)
    CreatedDate = models.DateTimeField(auto_now_add=True)
    #Prefix_link = models.CharField(max_length=1000, null=True, blank=True)
    StartDate = models.DateTimeField(null=True, blank=True)
    FinishedDate = models.DateTimeField(null=True, blank=True)
    #Enable = models.BooleanField()
    DownloadTime = models.IntegerField(null=True, blank=True)
    # this field use for storing the xpath to determine where need to go to next page
    PageParam = models.TextField()
    PageParam_rule = models.IntegerField(null=True, blank=True)
    #random.randint(1, 24)
    schedule_hour = models.IntegerField(
        null=True, blank=True, default=-1)
    # this field use for storing the xpath to determine when is the news
    ArticleLinkSelector = models.CharField(max_length=500)

    def __str__(self):
        return self.master_name.MasterName + ' --> ' + self.SiteName
        #return self.SiteName

class Sites_Documents(models.Model):
    site = models.ForeignKey(Sites, on_delete = models.CASCADE)
    document = models.ForeignKey(Documents, on_delete = models.CASCADE)

class SiteAttributes(models.Model):
    """
    this class use for storing need xpath of sites to get the right value
    """
    ATTRIBUTE_NAME_CHOICES = (
        ('TITLE', 'TITLE'),
        ('ABSTRACT', 'ABSTRACT'),
        ('CONTENT', 'CONTENT'),
        ('DATA_TIME', 'DATA_TIME'),
        ('DATA_TIME_STR', "DATA_TIME_STR"),
        ('AUTHOR', 'AUTHOR'),
        ('HEAD_PARAGRAPH', 'HEAD_PARAGRAPH'),
        ("VIEWCOUNT", "VIEWCOUNT"),
        ('TAGS', "TAGS"),
        ('IMAGE_DESC', "IMAGE_DESC"),
        ('IMAGE_URL', "IMAGE_URL")
    )

    TYPE_SELECTORS = (
        ('HTML','HTML'),
        ('JSON', 'JSON')
    )

    #site = models.ManyToManyField(Sites)#một site có nhiều attr

    root_site_name = models.ForeignKey(
       SiteMaster, on_delete=models.SET_NULL, null=True)#master site

    FieldName = models.CharField(
        max_length=50, choices=ATTRIBUTE_NAME_CHOICES)#tên của mục bài báo cần lấy

    Type_selectors = models.CharField(
        max_length=50, choices=TYPE_SELECTORS, default="HTML")

    FindJson = models.CharField(max_length=50, null=True, blank=True)

    Selectors = models.CharField(max_length=200)#cú pháp selector lấy 

    def __str__(self):
        return self.FieldName + ' ---> ' + self.root_site_name.MasterName

class DownloadWarning(models.Model):
    site = models.ForeignKey(Sites, on_delete=models.SET_NULL, null=True)
    Link = models.CharField(max_length=500)
    CreateDate = models.DateTimeField(auto_now_add=True)
    Reason = models.CharField(max_length=500, null=True, blank=True)
    IsFailed = models.BooleanField(default=False)

    def __str__(self):
        return self.Link

class ReadyCrawledDocument(models.Model):
    class Meta:
        unique_together = (('site', 'LinkDoc'),)
    site = models.ForeignKey(Sites, on_delete = models.SET_NULL, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    LinkDoc = models.CharField(
        max_length=2000, null=True, blank=True)

    def __str__(self):
        return str(self.create_date) + ' ' + self.LinkDoc

PREFIX_LINKS = {
	'https://www.reuters.com/' : 'https://www.reuters.com',
    'https://www.foxnews.com/' : 'https://www.foxnews.com'
}

PROXIES = ['http://113.167.218.169:8080', 'http://123.25.24.23:80',
           'http://14.170.154.10:8080', 'http://113.161.186.101:8080', 'http://113.161.207.99:60626']
USERS_AGENT = [
    'Mozilla/5.0 (Android; Mobile; rv:40.0) Gecko/40.0 Firefox/40.0'
    ,'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0'
    ,'Mozilla/5.0 (Macintosh; PPC Mac OS X x.y; rv:10.0) Gecko/20100101 Firefox/10.0'
    ,'Mozilla/5.0 (Android; Tablet; rv:40.0) Gecko/40.0 Firefox/40.0'
    ,'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0'
]