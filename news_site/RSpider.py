import requests
from lxml import html, etree
from requests_html import HTMLSession
from multiprocessing import Pool
from bs4 import BeautifulSoup
import threading
from io import StringIO, BytesIO
import lxml
import time
import json
import re
import random
from .preprocess import process_datetime, del_script
from itertools import cycle
import traceback
from django.utils import timezone
from datetime import datetime
import pytz
from .models import (Categories, Documents, DownloadWarning, Languages,
                     ReadyCrawledDocument, SiteAttributes, Sites, Sites_Documents, PREFIX_LINKS, PROXIES, USERS_AGENT)
import datetime
from requests.exceptions import HTTPError


#Cartoon
class AdvanceRSpider:
	def __init__(self, site):
		self.site = {
			'id' : site.id,
			'documents_list' : self.get_documents_list(site),
			'state' : 'New',
			'site_resource':{
				'type_response' : site.master_name.response_type,
				'site_master' : site.master_name.MasterName,
				'site_url' : site.SiteLink,
				'href_xpath' : site.ArticleLinkSelector,
				'next_page_href' : site.PageParam,
				'page_param_rule' : site.PageParam_rule
			}
		}
		self.mylock = threading.Lock()
		self.total_thread = 0
		self.requests_count = 1
		self.requests_settings = {
			'headers' : {'user-agent': 'Mozilla/5.0 (Android; Mobile; rv:40.0) Gecko/40.0 Firefox/40.0'},
			'cookies' : {'visit-month' : 'February'},
			'proxies' : {'http' : 'http://14.170.154.10:8080', 'https' : 'http://14.170.154.10:8080'},
			'requests_session' : requests.Session(),
			'period_reset' : 30
		}
		self.session = requests.Session()
		#s.mount('http://', HTTPAdapter(max_retries=retries))
		#self.session.proxies.update(self.requests_settings['proxies'])

		print('Crawling ' + str(site.SiteLink))

		#xpath cho từng field
		self.xpath = {
			'TITLE': '',
			'ABSTRACT': '',
			'CONTENT': '',
			'DATA_TIME': '',
			'AUTHOR': '',
			'HEAD_PARAGRAPH': '',
			'VIEWCOUNT': '',
			'TAGS': '',
			'DATA_TIME_STR': '',
			'IMAGE_DESC': '',
			'IMAGE_URL': ''
		}
		#loại xpath là json hay html
		self.type_xpath = {
			'TITLE': '',
			'ABSTRACT': '',
			'CONTENT': '',
			'DATA_TIME': '',
			'AUTHOR': '',
			'HEAD_PARAGRAPH': '',
			'VIEWCOUNT': '',
			'TAGS': '',
			'DATA_TIME_STR': '',
			'IMAGE_DESC': '',
			'IMAGE_URL': ''
		}
		#khi lấy xpath bằng json cần xác định đoạn javascript chứa field đó
		self.xpath_support = {
			'TITLE': '',
			'ABSTRACT': '',
			'CONTENT': '',
			'DATA_TIME': '',
			'AUTHOR': '',
			'HEAD_PARAGRAPH': '',
			'VIEWCOUNT': '',
			'TAGS': '',
			'DATA_TIME_STR': '',
			'IMAGE_DESC': '',
			'IMAGE_URL': ''
		}

	#thay đổi proxy và header theo một chu kỳ request thực hiện
	def change_settings_per_requests(self):
		if self.requests_count % self.requests_settings['period_reset'] == 0:
			print('Change settings requests')			
			previous_headers = self.requests_settings['headers']['user-agent']
			previous_proxy = self.requests_settings['proxies']['http']

			new_headers = random.choice(USERS_AGENT)
			while new_headers == previous_headers:
				new_headers = random.choice(USERS_AGENT)
			
			new_proxy = random.choice(PROXIES)
			while new_proxy == previous_proxy:
				new_proxy = random.choice(PROXIES)
			
			self.requests_settings['headers']['user-agent'] = new_headers
			self.requests_settings['proxies']['http'] = new_proxy
			self.requests_settings['proxies']['https'] = new_proxy			
			self.session.proxies.update(self.requests_settings['proxies'])

	#thay đổi thông số request
	def change_settings_requests(self, new_headers, new_proxy):
		self.requests_settings['headers']['user-agent'] = new_headers
		self.requests_settings['proxies']['http'] = new_proxy
		self.requests_settings['proxies']['https'] = new_proxy
		self.session.proxies.update(self.requests_settings['proxies'])

	#lấy các document mà site đã crawl được
	def get_documents_list(self, site):
		doc_dic = {}
		documents = site.documents.all()
		for i, doc in enumerate(documents):
			doc_dic[doc.LinkDoc] = i
		return doc_dic
	
	#Thay đổi trạng thái cho site
	def set_state(self, site_id, state):
		choices = {'New': 0,
			'Crawling': 1,
			'Stop': 2,
			'Crawling_Finished': 3}
		site_crawling = Sites.objects.get(id=site_id)
		site_crawling.State = choices[state]
		site_crawling.save()

	#khởi động crawl 
	def start(self):
		self.set_state(self.site['id'], 'Crawling')
		start_time = time.time()
		self.url_baiviet = []
		self.url_ready = []
		next_page = self.site['site_resource']['site_url']#lấy trang khởi tạo
		is_stop = False# dk dừng
		current_page = 1#khởi tạo trang đầu tiên
		baiviet = []#lưu biến kiểm tra trong db ready 
		while is_stop == False:
			got_href, is_baimoi, count_href = False, False, 0
			tmp_baiviet = self.get_href_links(next_page, current_page)#lấy các link bài viết
			for link in tmp_baiviet['links']:
				got_href = True
				count_href += 1
				if link in self.site['documents_list'] :#khi gặp lại các bài cũ thì dừng update
					is_stop = True
					break
				else:
					try:
						#kiểm tra link đã có trong ready chưa
						document_ready = ReadyCrawledDocument.objects.filter(site=Sites.objects.get(id=self.site['id']),LinkDoc=link).first()
						if document_ready is None: #nếu chưa thì thêm vào ready
							self.url_baiviet.append(link)
							baiviet = []
							is_baimoi = True#có bài mới trong trang này                        
							new_ready_crawled_document = ReadyCrawledDocument(site=Sites.objects.get(id=self.site['id']),LinkDoc=link)
							new_ready_crawled_document.save()
					except Exception as e:
						download_warning = DownloadWarning(site=Sites.objects.get(id=self.site['id']),Link=link,Reason=e)
						download_warning.save()
						print('Exception link-----------------------------------: ' + str(link))
			
			if '[NEXT_PAGE]' in self.site['site_resource']['next_page_href']:#khởi tạo trang tiếp theo để crawl
				if self.site['site_resource']['page_param_rule'] is not None:
					next_page = self.site['site_resource']["next_page_href"].replace(
                    	'[NEXT_PAGE]',
                    	str((current_page + 1)
                        	* self.site['site_resource']['page_param_rule'])
					)
				else:#các trang nhảy không theo 1,2 ,3, 4 mà theo rule khác
					next_page = self.site['site_resource']["next_page_href"].replace(
                    	'[NEXT_PAGE]',
                    	str(current_page + 1)
					)
				current_page += 1
			else:
				is_stop = True

			if is_baimoi == False:#nếu site không có bài mới trong ready
				baiviet.append('s')


			if got_href == False or len(baiviet) > 4: #nếu 4 site liên tiếp không có bài mới hoặc không còn bài viết
				is_stop = True			
			# if current_page == 15 or got_href == False or len(baiviet) > 4:
			#  	is_stop = True
			time.sleep(2)

		ready_crawled_documents = ReadyCrawledDocument.objects.filter(site__id=self.site['id'])#lấy các link crawl 
		site_attributes = SiteAttributes.objects.filter(root_site_name=Sites.objects.filter(id=self.site['id']).first().master_name)#lấy các attribute cần crawl của site
		for site_attribute in site_attributes:#lấy xpath của site
			self.xpath[site_attribute.FieldName] = site_attribute.Selectors#lấy các selector của attr
			self.type_xpath[site_attribute.FieldName] = site_attribute.Type_selectors#loại selector json hay html
			self.xpath_support[site_attribute.FieldName] = site_attribute.FindJson#vị trí trên javascipt
		self.url_ready = self.url_baiviet#ready_crawled_documents
		self.run()#Bắt đầu crawl site
		#self.single_thread()
		print("Total time --- %s seconds ---" % (time.time() - start_time))
		self.closed()#xóa các bài trong ready

	def error_handle_gethref(self, link_error, current_page):#khi có lỗi xảy ra khi crawlling
		proxy_pool = cycle(PROXIES)#các proxy có sẵn
		list_url = []
		response = None
		is_success = False
		for i in range(1, 11):#cho chạy 10 lần
			proxy = next(proxy_pool)#lấy proxy ngẫu nhiên
			try:
				user_agent = random.choice(USERS_AGENT)
				#response = requests.get(link_error, headers={'user-agent': user_agent}, proxies={"http":proxy, "https":proxy})
				headers = {"User-Agent":user_agent}
				#thay đổi proxy khác
				self.session.proxies.update({"http":proxy, "https":proxy})#thay đổi proxy
				self.session.headers.update(headers)

				response = self.session.get(link_error, timeout=9)

				if self.site['site_resource']['type_response'] == 'HTML':#Nếu response là html
					parser = etree.HTMLParser()
					tree = etree.parse(StringIO(response.text), parser)
					list_url = tree.xpath(str(self.site['site_resource']['href_xpath']) + '/@href')
				else:#response là json
					list_url = [res[str(self.site['site_resource']['href_xpath'])] for res in response.json()]
				
				if self.site['site_resource']['site_master'] in PREFIX_LINKS:#khi link không đầu đủ
					tmp_list = []
					for link in list_url:
						if (("http://" in link) or ("https://" in link)):#kiểm tra link đã có prefix
							tmp_list.append(link)#giữ nguyên
						else:#nếu thiếu prefix
							tmp_list.append(str(PREFIX_LINKS[self.site['site_resource']['site_master']] + link))#thêm prefix trc link
					list_url = tmp_list

				print('Page ' + str(current_page) + '(' + str(link_error) + ')' + ':' ,end = '')
				print(str(len(list_url)) + ' bài' )
				#self.change_settings_requests(user_agent, proxy)
				is_success = True
				print('Proxy server success:', end = '')
				print(proxy)
				break
			except:
				list_url = []
				print("Skipping. Connection error gethref")

		if is_success == False:
			self.save_warning_url(url=link_error, reason='403 Exception error')
		return list_url

	def handle_403(self, link_error):
		proxy_pool = cycle(PROXIES)
		response = None
		is_success = False
		for i in range(1, 11):
			proxy = next(proxy_pool)
			try:
				user_agent = random.choice(USERS_AGENT)
				headers = {"User-Agent":user_agent}
				self.session.proxies.update({"http":proxy, "https":proxy})
				self.session.headers.update(headers)
				response = self.session.get(link_error, timeout=9)
				print('Proxy server success:', end = '')
				print(proxy)
				is_success = True
				break
			except:
				print("403 error")
		if is_success == False:
			self.save_warning_url(url=link_error, reason='403 error')
		return response

	def get_href_links(self, url_next_page, current_page):
		result = {}
		response = None
		links = None
		try:
			response = self.session.get(url_next_page, timeout=9)
			if int(response.status_code) == 403:#gặp lỗi 403
				response = self.handle_403(url_next_page)

			if int(response.status_code) == 200:
				if self.site['site_resource']['type_response'] == 'HTML':#nếu loại response là html
					try:
						parser = etree.HTMLParser()
						tree = etree.parse(StringIO(response.text), parser)
						links = tree.xpath(str(self.site['site_resource']['href_xpath']) + '/@href')
					except:
						links = []
				else:
					try:
						links = [res[str(self.site['site_resource']['href_xpath'])] for res in response.json()]
					except:
						links = []

				if self.site['site_resource']['site_master'] in PREFIX_LINKS:
					tmp_list = []
					for link in links:
						if (("http://" in link) or ("https://" in link)):
							tmp_list.append(link)
						else:
							tmp_list.append(str(PREFIX_LINKS[self.site['site_resource']['site_master']] + link))
					result['links'] = tmp_list
				else:
					result['links'] = links
				
				result['message'] = 'success'
				print('Page ' + str(current_page) + '(' + str(url_next_page) + ')' + ':' ,end = '')
				print(str(len(links)) + ' bài' )
			else:
				result['message'] = 'failure'
				result['links'] = []
				self.save_warning_url(url=url_next_page, reason='Next Page Error')

		except requests.exceptions.RequestException as e:
			result['message'] = 'error'
			print('RequestException -------------------')
			result['links'] = self.error_handle_gethref(url_next_page, current_page)
		except:
			self.save_warning_url(url=url_next_page, reason='Other Error')
			result['message'] = 'error'
			result['links'] = []
		return result

	def get_documents(self, url):#crawl document
		try:
			response = self.session.get(url, timeout=9)
			try:
				parser = etree.HTMLParser()
				tree = etree.parse(StringIO(response.text), parser)
			except:
				pass

			title = None
			try:
				title = tree.xpath(self.xpath['TITLE'])
				title = etree.tostring(title[0])
			except:
				title = 'missing'
			
			date_time = None
			if self.type_xpath['DATA_TIME_STR'] == 'JSON':#field cần lấy ở dạng json
				try:
					soup = BeautifulSoup(response.text, 'html.parser')
					script_date = soup.find('script', type=self.xpath_support['DATA_TIME_STR'])#lấy đúng script chứa field
					json_date = json.loads(del_script(script_date), strict=False)#parse thành json
					numb_key_json =  self.xpath['DATA_TIME_STR'].split('/')
					prevz_res = json_date
					for key in numb_key_json:#lấy data của json
						prevz_res = prevz_res[key]
					date_time = prevz_res
				except:
					date_time = 'missing'

			else:#field ở dạng html
				try:
					date_time = tree.xpath(self.xpath['DATA_TIME_STR'])
					date_time =  etree.tostring(date_time[0])
				except:
					date_time =  'missing'

			content = None
			try:
				content = tree.xpath(self.xpath['CONTENT'])
				content = etree.tostring(content[0])
			except:
				content = 'missing'

			tags = None
			try:
				tags = tree.xpath(self.xpath['TAGS'])
				tags =  etree.tostring(tags[0])
			except:
				tags = 'missing'

			author = None
			try:
				author = tree.xpath(self.xpath['AUTHOR'])
				author = etree.tostring(author[0])
			except:
				author = 'missing'

			document = {
				'site': Sites.objects.get(id=self.site['id']),
				'LinkDoc': url,
				'Title': 'missing',
				'Abstract': 'missing',
				'Author': 'missing',
				'Content': 'missing',
				'DateDoc': None,
				'date_time_str': 'missing',
				'Tags': 'missing',
				'JsonDoc': 'missing',
				'ViewCount': -1,
				'head_paragraph': 'missing',
				'image_descs': 'missing',
				'image_urls': 'missing',
        	}


			if title != 'missing':
				document['Title'] = str(title)[2:-1]
			else:
				document['Title'] = str(title)

			if date_time != 'missing':
				if self.type_xpath['DATA_TIME_STR'] != 'JSON':
					document['date_time_str'] = str(date_time)[2:-1]
					process_date = process_datetime(str(date_time)[2:-1], self.site['site_resource']['site_master'])
					document['DateDoc'] = process_date
				else:
					document['date_time_str'] = str(date_time)
					process_date = process_datetime(str(date_time), self.site['site_resource']['site_master'])
					document['DateDoc'] = process_date
			else:
				document['date_time_str'] = str(date_time)
			
			if content != 'missing':
				document['Content'] = str(content)[2:-1]
			else:
				document['Content'] = str(content)
			
			if tags != 'missing':
				document['Tags'] = str(tags)[2:-1]
			else:
				document['Tags'] = str(tags)	

			if author != 'missing':
				document['Author'] = str(author)[2:-1]
			else:			
				document['Author'] = str(author)

			document_exist = Documents.objects.filter(LinkDoc=document['LinkDoc']).first()
			if document_exist is not None:
				document_site_exist = Sites_Documents.objects.filter(site = document['site'], document = document_exist).first()
				if document_site_exist is  None:
					Sites_Documents(document=document_exist, site=document['site']).save()
			else:
				d = Documents(
                    LinkDoc=document['LinkDoc'],
                    Title=document['Title'],
                    Abstract=document['Abstract'],
                    Author=document['Author'],
                    Content=document['Content'],
                    DateDoc=document['DateDoc'],
                    date_time_str=document['date_time_str'],
                    Tags=document['Tags'],
                    ViewCount=document['ViewCount'],
                    JsonDoc=document['JsonDoc']
				)
				d.save()
				Sites_Documents(document=d, site=document['site']).save()

			print('Success:', end = '')
			print(url)
		except requests.exceptions.RequestException as e:
			print('Failure:', end = '')
			print(url)
			print(e)
			self.save_warning_url(url=url, reason=e)
		except Exception as e:
			self.save_warning_url(url=url, reason=e)

	def delete_readydoc(self, site_id):
		 _ = ReadyCrawledDocument.objects.filter(site__id=site_id).delete()

	def single_thread(self):
		for url in self.url_baiviet:
			self.get_documents(url)

	def closed(self):
		print('--------------------DELETE READY DOCUMENTS---------------------------')
		self.delete_readydoc(self.site['id'])
		self.set_state(self.site['id'], 'Crawling_Finished')

	def threading_crawl(self, id):
		for link in self.linksthread[id]:
			self.get_documents(link)
			#time.sleep(1 + random.uniform(0,1))
			time.sleep(1)
		self.mylock.acquire(timeout=5)
		self.total_thread -= 1
		self.mylock.release()

	def save_warning_url(self, url, reason):
		download_warning = DownloadWarning(site=Sites.objects.get(id=self.site['id']),Link=url,Reason=reason)
		download_warning.save()

	def run(self):
		linkcount = len(self.url_baiviet)
		threadCount = linkcount // 10
		if threadCount > 4:
			threadCount = 4
		elif threadCount == 0:
			threadCount = 1
		self.linksthread = []
		no_linkthread = linkcount // threadCount

		for i in range(threadCount):
			self.linksthread.append(self.url_baiviet[:no_linkthread])
			if len(self.url_baiviet) > no_linkthread:
				self.url_baiviet = self.url_baiviet[no_linkthread:]
			self.mylock.acquire(timeout=5)
			self.total_thread += 1
			self.mylock.release()
			thread1 = threading.Thread(target=self.threading_crawl, args=([i]))
			thread1.start()

		time_limit = 1
		prev_numb_docs = len(list(Sites_Documents.objects.filter(site=Sites.objects.filter(id=self.site['id']).first())))
		while self.total_thread > 0:
			time.sleep(1)
			time_limit += 1
			if time_limit % 180 == 0:
				cur_numb_docs = len(list(Sites_Documents.objects.filter(site=Sites.objects.filter(id=self.site['id']).first())))
				if prev_numb_docs == cur_numb_docs:
					self.save_warning_url(self, url = self.site['site_resource']['site_url'], reason='Có lỗi xảy ra khi đang crawl, không có sự thay đổi về số lượng bài viết')
					break
				else:
					prev_numb_docs = cur_numb_docs

###########################################################################################################

class RSpider:
	def __init__(self, site):
		self.site = {
			'id' : site.id,
			'documents_list' : self.get_documents_list(site),
			'state' : 'New',
			'site_resource':{
				'type_response' : site.master_name.response_type,
				'site_master' : site.master_name.MasterName,
				'site_url' : site.SiteLink,
				'href_xpath' : site.ArticleLinkSelector,
				'next_page_href' : site.PageParam,
				'page_param_rule' : site.PageParam_rule
			}
		}
		self.mylock = threading.Lock()
		self.total_thread = 0
		print('Crawling ' + str(site.SiteLink))
		self.xpath = {
			'TITLE': '',
			'ABSTRACT': '',
			'CONTENT': '',
			'DATA_TIME': '',
			'AUTHOR': '',
			'HEAD_PARAGRAPH': '',
			'VIEWCOUNT': '',
			'TAGS': '',
			'DATA_TIME_STR': '',
			'IMAGE_DESC': '',
			'IMAGE_URL': ''
		}

	def get_documents_list(self, site):
		doc_dic = {}
		documents = site.documents.all()
		for i, doc in enumerate(documents):
			doc_dic[doc.LinkDoc] = i
		return doc_dic

	def set_state(self, site_id, state):
		choices = {'New': 0,
			'Crawling': 1,
			'Stop': 2,
			'Crawling_Finished': 3}
		site_crawling = Sites.objects.get(id=site_id)
		site_crawling.State = choices[state]
		site_crawling.save()

	def start(self):
		self.set_state(self.site['id'], 'Crawling')
		start_time = time.time()
		self.url_baiviet = []
		self.url_ready = []
		next_page = self.site['site_resource']['site_url']
		is_stop = False
		current_page = 1
		baimoi = []
		while is_stop == False:
			got_href, is_baimoi, count_href = False, False, 0
			tmp_baiviet = self.get_href_links(next_page)
			print('Page ' + str(current_page) + '(' + str(next_page) + ')' + ':' ,end = '')
			print(len(tmp_baiviet['links']))
			for link in tmp_baiviet['links']:
				got_href = True
				count_href += 1
				if link in self.site['documents_list'] :
					is_stop = True
					break
				else:
					try:
						document_ready = ReadyCrawledDocument.objects.filter(site=Sites.objects.get(id=self.site['id']),LinkDoc=link).first()
						if document_ready is None:
							baimoi = []
							self.url_baiviet.append(link)
							is_baimoi = True                        
							new_ready_crawled_document = ReadyCrawledDocument(site=Sites.objects.get(id=self.site['id']),LinkDoc=link)
							new_ready_crawled_document.save()
					except Exception as e:
						download_warning = DownloadWarning(site=Sites.objects.get(id=self.site['id']),Link=link,Reason=e)
						download_warning.save()
						print('Exception link-----------------------------------: ' + str(link))
			
			if '[NEXT_PAGE]' in self.site['site_resource']['next_page_href']:
				if self.site['site_resource']['page_param_rule'] is not None:
					next_page = self.site['site_resource']["next_page_href"].replace(
                    	'[NEXT_PAGE]',
                    	str((current_page + 1)
                        	* self.site['site_resource']['page_param_rule'])
					)
				else:
					next_page = self.site['site_resource']["next_page_href"].replace(
                    	'[NEXT_PAGE]',
                    	str(current_page + 1)
					)
					current_page += 1
			else:
				is_stop = True

			if is_baimoi == False:
				baimoi.append('s')

			if got_href == False or len(baimoi) > 4:
				is_stop = True			
			# if current_page == 10:
			#  	is_stop = True

		ready_crawled_documents = ReadyCrawledDocument.objects.filter(site__id=self.site['id'])
		site_attributes = SiteAttributes.objects.filter(site__id=self.site['id'])
		for site_attribute in site_attributes:
			self.xpath[site_attribute.FieldName] = site_attribute.Selectors

		self.url_ready = self.url_baiviet#ready_crawled_documents
		#self.multi_processCrawl(numb_process=2)
		self.run()# MultiThread 
		#self.single_process()
		print("Total time --- %s seconds ---" % (time.time() - start_time))
		self.closed()
		
	def get_href_links(self, url_next_page):
		result = {}
		try:
			session = HTMLSession()
			response = session.get(url_next_page)
			links = None
			if self.site['site_resource']['type_response'] == 'HTML':
				links = response.html.xpath(str(self.site['site_resource']['href_xpath']) + '/@href')
			else:
				links = None
			result['message'] = 'success'
			if self.site['site_resource']['site_master'] in PREFIX_LINKS:
				result['links'] = [str(PREFIX_LINKS[self.site['site_resource']['site_master']] + link)  for link in links]
			else:
				result['links'] = links
		except requests.exceptions.RequestException as e:
			result['message'] = 'error'
			result['links'] = []
			print(e)
		return result

	def get_documents(self, url):
		try:
			session = HTMLSession()
			response = session.get(url)

			title = response.html.xpath(self.xpath['TITLE'])
			if len(title) > 0:
				try:
					title = response.html.xpath(self.xpath['TITLE'])[0].html
				except:
					pass
			date_time = response.html.xpath(self.xpath['DATA_TIME_STR'])
			if len(date_time) > 0:
				try:
					date_time = response.html.xpath(self.xpath['DATA_TIME_STR'])[0].html
				except:
					pass	
			content = response.html.xpath(self.xpath['CONTENT'])
			if len(content) > 0:
				try:
					content = response.html.xpath(self.xpath['CONTENT'])[0].html
				except:
					pass	
			tags = response.html.xpath(self.xpath['TAGS'])
			if len(tags) > 0:
				try:
					tags = response.html.xpath(self.xpath['TAGS'])[0].html	
				except:
					pass
			author = response.html.xpath(self.xpath['AUTHOR'])
			if len(author) > 0:
				try:
					author = response.html.xpath(self.xpath['AUTHOR'])[0].html 
				except:
					pass

			document = {
				'site': Sites.objects.get(id=self.site['id']),
				'LinkDoc': url,
				'Title': 'missing',
				'Abstract': 'missing',
				'Author': 'missing',
				'Content': 'missing',
				'DateDoc': None,
				'date_time_str': 'missing',
				'Tags': 'missing',
				'JsonDoc': 'missing',
				'ViewCount': -1,
				'head_paragraph': 'missing',
				'image_descs': 'missing',
				'image_urls': 'missing',
        	}
			document['Title'] = title
			document['date_time_str'] = date_time
			document['Content'] = content
			document['Tags'] = tags
			document['Author'] = author

			document_exist = Documents.objects.filter(LinkDoc=document['LinkDoc']).first()
			if document_exist is not None:
				document_site_exist = Sites_Documents.objects.filter(site = document['site'], document = document_exist).first()
				if document_site_exist is  None:
					Sites_Documents(document=document_exist, site=document['site']).save()
			else:
				d = Documents(
                    LinkDoc=document['LinkDoc'],
                    Title=document['Title'],
                    Abstract=document['Abstract'],
                    Author=document['Author'],
                    Content=document['Content'],
                    DateDoc=document['DateDoc'],
                    date_time_str=document['date_time_str'],
                    Tags=document['Tags'],
                    ViewCount=document['ViewCount'],
                    JsonDoc=document['JsonDoc']
				)
				d.save()
				Sites_Documents(document=d, site=document['site']).save()

			print('Success:', end = '')
			print(url)
		except requests.exceptions.RequestException as e:
			print('Failure:', end = '')
			print(url)
			print(e)

	def delete_readydoc(self, site_id):
		 _ = ReadyCrawledDocument.objects.filter(site__id=site_id).delete()

	def closed(self):
		print('--------------------DELETE READY DOCUMENTS---------------------------')
		self.delete_readydoc(self.site['id'])
		self.set_state(self.site['id'], 'Crawling_Finished')

	def multi_processCrawl(self, numb_process = 2):
		p = Pool(processes=numb_process)
		p.map(self.get_documents, self.url_ready)
		p.terminate()
		p.join()

	def single_thread(self):
		for url in self.url_baiviet:
			self.get_documents(url)

	def threading_crawl(self, id):
		for link in self.linksthread[id]:
			self.get_documents(link)

		self.mylock.acquire(timeout=5)
		self.total_thread -= 1
		self.mylock.release()

	def run(self):
		linkcount = len(self.url_baiviet)
		threadCount = linkcount // 10
		if threadCount > 4:
			threadCount = 4
		elif threadCount == 0:
			threadCount = 1
		self.linksthread = []
		no_linkthread = linkcount // threadCount

		for i in range(threadCount):
			self.linksthread.append(self.url_baiviet[:no_linkthread])
			if len(self.url_baiviet) > no_linkthread:
				self.url_baiviet = self.url_baiviet[no_linkthread:]
			self.mylock.acquire(timeout=3)
			self.total_thread += 1
			self.mylock.release()
			thread1 = threading.Thread(target=self.threading_crawl, args=([i]))
			thread1.start()

		while self.total_thread > 0:
			time.sleep(1)



# 3584 bài : ~3031s (sequences) (chưa có time ngắt giữa các lần crawl)
# 3596 bài : ~982s (4 threads) (chưa có time ngắt giữa các lần crawl)
# 32768 bài: ~11066s (4 thread) (chưa có time ngắt giữa các lần crawl)


