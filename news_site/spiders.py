import json
import re
import time
from datetime import datetime
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError
import pytz
import scrapy
from sty import RgbFg, bg, ef, fg, rs

from .file_rw import (
    add_stop, check_stop, get_state, read_state, remove_stop, write_state)
from .models import (Categories, Documents, DownloadWarning, Languages,
                     ReadyCrawledDocument, SiteAttributes, Sites, Sites_Documents)

import datetime

#fg.set_style('green', RgbFg(28, 255, 55))


class AdvanceSpider(scrapy.Spider):
    handle_httpstatus_list = [404]
    # custom_settings = {
    #     'DOWNLOAD_DELAY': 0.25,
    #     'CONCURRENT_REQUESTS': 100,
    #     'HTTPERROR_ALLOWED_CODES': [500]
    # }

    #RETRY_ENABLED = False
	#theo dõi tình hình crawl của một site
    def set_count_news(self, site_id=None, downloadcount=None, succcount=None):
        site = Sites.objects.get(id=site_id)
        if downloadcount:
            site.DownloadCount = downloadcount
        if succcount:
            site.SuccCount = succcount
        site.save()
		
	#theo dõi trạng thái hoạt động của site
    def set_state(self, site_id, state):
        choices = {'New': 0,
                   'Crawling': 1,
                   'Stop': 2,
                   'Crawling_Finished': 3}
        site = Sites.objects.get(id=site_id)
        site.State = choices[state]
        site.save()

	#Lấy các documents của của site đã được crawl
    def get_documents_list(self, site):
        doc_dic = {}
        documents = site.documents.all()
        for i, doc in enumerate(documents):
            doc_dic[doc.LinkDoc] = i
        return doc_dic
	
	#Xóa các document trong bảng ReadySite
    def delete_readydoc(self, site_id):
        _ = ReadyCrawledDocument.objects.filter(site__id=site_id).delete()
	
	##Khởi tạo các sites cần crawl
    def __init__(self):
        self.sites = []
        now = datetime.datetime.now()#thời gian hiện tại
        print(Sites.objects.filter(schedule_hour=now.minute))
        for site in Sites.objects.filter(schedule_hour=now.minute):#lặp qua các site cần tới time crawl
            if site.State != 1: 
                self.sites.append({'id': site.id, #
                                'count_total_links': 0,
                                'count_success_links': 0,
                                'documents_list': self.get_documents_list(site), #
                                'url_list': {}, # url các bài báo
                                'state': 'New', #
                                'site_resource': {
                                    'site_url': site.SiteLink, # link của site con
                                    'href_xpath': site.ArticleLinkSelector, # xpath các bài báo
                                    'next_page_href': site.PageParam, # xpath các trang mục chủ đề
                                    'site_catagory_id': site.category.id, # loãi tin tức của site
                                    'page_param_rule': site.PageParam_rule # xpath đặc biệt
                                }
                                })
    def start_requests(self):
        for site in self.sites: #lặp qua các site cần crawl 
            self.set_state(site['id'], 'Crawling') #khởi tạo trạng thái đang Crawling với chúng
            yield scrapy.Request( # gửi yêu cầu bằng url của site
                url=site['site_resource']['site_url'],
                meta={
                    'current_page': 1, # trang đầu tiên của site
                    'site': site,
                    'stop' : 10 # 
                },
                callback=self.href_getter, # gọi phương thức thực hiện lấy các bài báo site đó 
                dont_filter=True
            )

    def href_getter(self, response):
        is_stop, got_href, is_baimoi, count_href, next_page = False, False, False, 0, ''
        #print('Status ' + str(response.meta['current_page']) + ': ' + str(response.status))
        #if response.status == 200:
        print('Number Link news:' ,end = '')
        print(len(response.xpath(response.meta['site']['site_resource']['href_xpath'])))
        for item in response.xpath(response.meta['site']['site_resource']['href_xpath']):
            if Sites.objects.get(id=response.meta['site']['id']).State == 2:
                self.delete_readydoc(response.meta['site']['id'])
                return
            got_href = True
            count_href += 1
            news_url = response.urljoin(item.css('::attr(href)').get())
            if news_url in response.meta['site']['documents_list'] :
                is_stop = True
                break
            else:
                try:
                    document_ready = ReadyCrawledDocument.objects.filter(site=Sites.objects.get(id=response.meta['site']['id']),LinkDoc=news_url).first()
                    if document_ready is None:
                        is_baimoi = True                        
                        new_ready_crawled_document = ReadyCrawledDocument(site=Sites.objects.get(id=response.meta['site']['id']),LinkDoc=news_url)
                        new_ready_crawled_document.save()
                except Exception as e:
                    download_warning = DownloadWarning(site=Sites.objects.get(id=response.meta['site']['id']),Link=news_url,Reason=e)
                    download_warning.save()
                    print('Exception link-----------------------------------: ' + str(news_url))
        ready_crawled_documents = ReadyCrawledDocument.objects.filter(site__id=response.meta['site']['id'])
        if '[NEXT_PAGE]' in response.meta['site']['site_resource']["next_page_href"] and got_href == True:
            if response.meta['site']['site_resource']['page_param_rule']:
                next_page = response.meta['site']['site_resource']["next_page_href"].replace(
                    '[NEXT_PAGE]',
                    str((response.meta["current_page"] + 1)
                        * response.meta['site']['site_resource']['page_param_rule'])
                )
            else:
                next_page = response.meta['site']['site_resource']["next_page_href"].replace(
                    '[NEXT_PAGE]',
                    str(response.meta["current_page"] + 1)
                )
        else:
            is_stop = True 
        
        # if  response.meta["current_page"] > response.meta["stop"]:
        #     is_stop = True

        # if is_baimoi == False:
        #     is_stop = True

        if response.request.url == response.meta['site']['site_resource']['site_url'] and response.meta["current_page"] != 1:
            is_stop = True
        if not is_stop and got_href:
            response.meta["current_page"] += 1
            yield scrapy.Request(
                next_page,
                meta=response.meta,
                callback=self.href_getter,
                dont_filter=True
                #errback=self.errback_httpbin
            )
        else:

            xpath = {
                'TITLE': '',
                'ABSTRACT': '',
                'CONTENT': '',
                'DATA_TIME': '',
                'AUTHOR': '',
                'HEAD_PARAGRAPH': '',
                'VIEWCOUNT': '',
                'TAGS': '',
                'DATA_TIME_STR': '',
                'IMAGE_DESC': '',
                'IMAGE_URL': ''
            }
            ready_crawled_documents = ReadyCrawledDocument.objects.filter(
                site__id=response.meta['site']['id']).order_by('create_date')
            site_attributes = SiteAttributes.objects.filter(site__id=response.meta['site']['id'])

            for site_attribute in site_attributes:
                xpath[site_attribute.FieldName] = site_attribute.Selectors

            count_href = 0
            for i, ready_crawled_document in enumerate(ready_crawled_documents):
                count_href += 1
                if Sites.objects.get(id=response.meta['site']['id']).State == 2:
                    self.delete_readydoc(response.meta['site']['id'])
                    return

                yield scrapy.Request(
                    url=ready_crawled_document.LinkDoc,
                    meta={
                        'xpath': xpath,
                        'ready_crawled_document': ready_crawled_document,
                        'site': response.meta['site']
                    },
                    callback=self.document_getter,
                    dont_filter=True
                )
            #print('Count Href: ' + str(count_href))
            self.set_state(response.meta['site']['id'], 'Crawling_Finished')


    def errback_httpbin(self, failure):
        if isinstance(failure.value, HttpError) == True:
            print(failure.request.url)
            print(failure.request.meta)
            response = failure.request
            xpath = {
                'TITLE': '',
                'ABSTRACT': '',
                'CONTENT': '',
                'DATA_TIME': '',
                'AUTHOR': '',
                'HEAD_PARAGRAPH': '',
                'VIEWCOUNT': '',
                'TAGS': '',
                'DATA_TIME_STR': '',
                'IMAGE_DESC': '',
                'IMAGE_URL': ''
            }
            ready_crawled_documents = ReadyCrawledDocument.objects.filter(
                site__id=response.meta['site']['id']).order_by('create_date')
            site_attributes = SiteAttributes.objects.filter(site__id=response.meta['site']['id'])

            for site_attribute in site_attributes:
                xpath[site_attribute.FieldName] = site_attribute.Selectors

            count_href = 0
            for i, ready_crawled_document in enumerate(ready_crawled_documents):
                count_href += 1
                if Sites.objects.get(id=response.meta['site']['id']).State == 2:
                    self.delete_readydoc(response.meta['site']['id'])
                    return
                yield scrapy.Request(
                    url=ready_crawled_document.LinkDoc,
                    meta={
                        'xpath': xpath,
                        'ready_crawled_document': ready_crawled_document,
                        'site': response.meta['site']
                    },
                    callback=self.document_getter,
                    dont_filter=True
                )
            #print('Count Href: ' + str(count_href))
            self.set_state(response.meta['site']['id'], 'Crawling_Finished')

    #Thực hiện lấy chi tiết thông tin của bài báo
    def document_getter(self, response):
        document = {
            'site': Sites.objects.get(id=response.meta['site']['id']),
            'LinkDoc': response.request.url,
            'Title': 'missing',
            'Abstract': 'missing',
            'Author': 'missing',
            'Content': 'missing',
            'DateDoc': None,
            'date_time_str': 'missing',
            'Tags': 'missing',
            'JsonDoc': 'missing',
            'ViewCount': -1,
            'head_paragraph': 'missing',
            'image_descs': 'missing',
            'image_urls': 'missing',
        }
        try:
            if response.meta['xpath']["TITLE"]:
                try:
                    document['Title'] = str(response.xpath(response.meta['xpath']["TITLE"]).get())
                except:
                    pass
            if response.meta['xpath']["ABSTRACT"]:
                try:
                    document['Abstract'] = str(response.xpath(response.meta['xpath']["ABSTRACT"]).get())
                except:

                    pass
            if response.meta['xpath']["AUTHOR"]:
                try:
                    document['Author'] = str(response.xpath(response.meta['xpath']["AUTHOR"]).get())
                except:
                    pass
            if response.meta['xpath']["VIEWCOUNT"]:
                try:
                    document['ViewCount'] = int(response.xpath(
                        response.meta['xpath']["VIEWCOUNT"]).get())
                except:
                    pass
            if response.meta['xpath']["DATA_TIME_STR"]:
                try:
                    document['date_time_str'] = str(response.xpath(
                        response.meta['xpath']["DATA_TIME_STR"]).get())
                except:
                    pass
            if response.meta['xpath']["CONTENT"]:
                try:
                    document['Content'] = str(response.xpath(response.meta['xpath']["CONTENT"]).get())
                except:
                    pass
            if response.meta['xpath']["DATA_TIME"]:
                try:
                    d_re = r'(\d+[\s/-]\d+[\s/-]\d+)'
                    date_time = response.xpath(response.meta['xpath']["DATA_TIME"]).get().replace(
                        '-', '/').replace(' ', '/')
                    date_time = re.search(d_re, date_time).group(1)
                    date_time = datetime.strptime(
                        date_time, '%d/%m/%Y', tzinfo=pytz.UTC)
                    document['DateDoc'] = date_time
                except:
                    pass
            if response.meta['xpath']["TAGS"]:
                try:
                    tags = ''
                    for paragraph in response.xpath(response.meta['xpath']["TAGS"]):
                        if paragraph.get():
                            tags += paragraph.get() + ','
                    document['Tags'] = tags[0:-1]
                except:
                    pass
            if response.meta['xpath']["HEAD_PARAGRAPH"]:
                try:
                    head_paragraph_xpaths = ''
                    for paragraph in response.xpath(response.meta['xpath']["HEAD_PARAGRAPH"]):
                        if paragraph.get():
                            head_paragraph_xpaths += paragraph.get() + '\n'
                    document['head_paragraph'] = head_paragraph_xpaths
                except:
                    pass
            if response.meta['xpath']["IMAGE_URL"]:
                try:
                    image_urls = ''
                    for image_url in response.xpath(response.meta['xpath']["IMAGE_URL"]):
                        if image_url.get():
                            image_urls += image_url.get() + '\n'
                    document['image_urls'] = image_urls
                except:
                    pass
            if response.meta['xpath']["IMAGE_DESC"]:
                try:
                    image_descs = ''
                    for image_desc in response.xpath(response.meta['xpath']["IMAGE_DESC"]):
                        if image_desc.get():
                            image_descs += image_desc.get() + '\n'
                    document['image_descs'] = image_descs
                except:
                    pass

            JsonDoc = {
                'head_paragraph': document['head_paragraph'],
                'image_descs': document['image_descs'],
                'image_urls': document['image_urls']
            }
            document_exist = Documents.objects.filter(LinkDoc=document['LinkDoc']).first()
            if document_exist is not None:
                document_site_exist = Sites_Documents.objects.filter(site = document['site'], document = document_exist).first()
                if document_site_exist is  None:
                    Sites_Documents(document=document_exist, site=document['site']).save()
            else:
                d = Documents(
                    LinkDoc=document['LinkDoc'],
                    Title=document['Title'],
                    Abstract=document['Abstract'],
                    Author=document['Author'],
                    Content=document['Content'],
                    DateDoc=document['DateDoc'],
                    date_time_str=document['date_time_str'],
                    Tags=document['Tags'],
                    ViewCount=document['ViewCount'],
                    JsonDoc=str(JsonDoc)
                )
                d.save()
                Sites_Documents(document=d, site=document['site']).save()
            response.meta['ready_crawled_document'].delete()
        except Exception as e:
            #print('Exception document-----------------------------------: ' + str(document['LinkDoc']))
            download_warning = DownloadWarning(
                site=document['site'],
                Link=document['LinkDoc'],
                Reason=e)
            download_warning.save()
        finally:
            pass
            #response.meta['ready_crawled_document'].delete()

    #Hàm được gọi khi trước khi spider kết thúc
    #Thực hiện xoá các documents được đưa vào ready site
    def closed(self, reason):
        print('--------------------DELETE READY DOCUMENTS---------------------------')
        for site in self.sites:
            self.delete_readydoc(site['id'])

#https://stackoverflow.com/questions/39902287/how-to-avoid-ban-when-uses-scrapy
#https://pkmishra.dev/blog/2013/03/18/how-to-run-scrapy-with-TOR-and-multiple-browser-agents-part-1-mac/