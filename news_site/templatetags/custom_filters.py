from django.template import Library

register = Library()
@register.simple_tag
def lookup(dic, key):
    return dic[int(key)]
