import datetime
import json
import time
from multiprocessing import Process, Queue
import threading
import scrapy
import scrapy.crawler as crawler
from scrapy.crawler import CrawlerProcess
from apscheduler.schedulers.background import BackgroundScheduler
from django.contrib.auth.models import User
from news_site.models import (Categories, Documents, DownloadWarning,
                              Languages, ReadyCrawledDocument, SiteAttributes,
                              SiteMaster, Sites, Sites_Documents)
from scrapy.utils.log import configure_logging
from twisted.internet import reactor
import random

from .file_rw import get_state, init_state, remove_stop
from .spiders import AdvanceSpider
from .preprocess import striphtml, preprocess, process_noise, MarkSection, display_document
from .RSpider import RSpider, AdvanceRSpider
from django.db.models import Q


Queue_site = []
def setup_process(spider, site_id=None):
    # def f(q):
    #     try:
    #         configure_logging()
    #         runner = crawler.CrawlerRunner()
    #         deferred = None
    #         if site_id:
    #             site = Sites.objects.get(id=int(site_id))
    #             deferred = runner.crawl(spider, site=site)
    #         else:
    #             deferred = runner.crawl(spider)
    #         deferred.addBoth(lambda _: reactor.stop())
    #         reactor.run()
    #         q.put(None)

    #     except Exception as e:
    #         q.put(e)
    # q = Queue()
    # p = Process(target=f, args=(q,))
    # p.start()
    # result = q.get()
    # p.join()

    # if result is not None:
    #     raise result
    def f(q):
        try:
            now = datetime.datetime.now()
            site_crawl = []

            for site in Sites.objects.filter(schedule_hour=now.minute):
                if site.State != 1 and site.State != 4: # site đang chờ hoặc đang crawling bởi process #
                    site.State = 4 
                    site.save()
                    site_crawl.append(site)

            for site in site_crawl:
                #if site.State != 1:
                s = spider(site)
                s.start()
            q.put(None)
        except Exception as e:
            q.put(e)
    
    q = Queue()
    p = Process(target=f, args=(q,))
    p.start()
    result = q.get()
    p.join()

    if result is not None:
        raise result
    # global Queue_site
    # now = datetime.datetime.now()
    # if len(Queue_site) == 0:
    #     for site in Sites.objects.filter(schedule_hour=now.minute):
    #         Queue_site.append(site)
    #     while len(Queue_site) > 0:
    #         s = spider(Queue_site.pop(0))
    #         s.start()
    # else:
    #     for site in Sites.objects.filter(schedule_hour=now.minute):
    #         Queue_site.append(site)


def daily_scrawler():
    print('set daily crawling !!!')
    setup_process(AdvanceRSpider)


global scheduler
scheduler = BackgroundScheduler()
scheduler.start()
scheduler.add_job(
    daily_scrawler,
    'interval', max_instances = 24,
 #   hours=1)
 minutes=1)
print('Time Minute Now ---------- ',datetime.datetime.now().minute)


def get_crawled_sites():
    now = datetime.datetime.now()
    sites = Sites.objects.filter(schedule_hour=now.hour)
    for site in sites:
        try:
            if get_state(site.id):
                if get_state(site.id)['state'] != 'crawling':
                    remove_stop(site.id)
                    outer_crawl(site.id)

            else:
                remove_stop(site.id)
                outer_crawl(site.id)
            time.sleep(10)
        except Exception as e:
            print('ERROR :: => ', e)


def outer_crawl(site_id=None, **kwargs):
    if site_id:
        init_state(site_id)
        setup_process(AdvanceSpider, site_id)
    else:
        site_id = int(kwargs.get('site_id'))
        init_state(site_id)
        setup_process(AdvanceSpider, site_id)


map_master_site = {
    'https://www.globaltimes.cn/' : 'globaltimes',
    'https://www.reuters.com/' : 'reuters',
    'https://www.cnbc.com/' : 'cnbc',
    'https://www.foxnews.com/' : 'foxnews',
    'https://www.japantimes.co.jp/' : 'japantimes'
}


def load_documents(category="all", page=1):
    #documents_query = Sites.objects.filter(master_name=SiteMaster.objects.filter(MasterName='https://www.reuters.com/').first()).first().documents.all().order_by('-DateDoc')
    documents_query = []
    documents_links = []
    cate_request = None
    if category == "all":
        documents_links = list(Documents.objects.values_list('LinkDoc', flat=True))
    elif category == "politic":
        cate_request = Categories.objects.filter(CateName="POLITIC").first()
    elif category == "world":
        cate_request = Categories.objects.filter(CateName="WORLD").first()
    elif category == "sports":
        cate_request = Categories.objects.filter(CateName="SPORTS").first()
    elif category == "business":
        cate_request = Categories.objects.filter(CateName="BUSINESS").first()
    elif category == "technology":
        cate_request = Categories.objects.filter(CateName="TECHNOLOGY").first()
    elif category == "science":
        cate_request = Categories.objects.filter(CateName="SCIENCE").first()
    elif category == "health":
        cate_request = Categories.objects.filter(CateName="HEALTH").first()
    elif category == "life":
        cate_request = Categories.objects.filter(CateName="LIFE").first()
    elif category == "opinion":
        cate_request = Categories.objects.filter(CateName="OPINION").first()
    

    if cate_request is not None:
        for site_category in list(Sites.objects.filter(category=cate_request).values_list("SiteLink", flat=True)):
            try:
                documents_links.extend(list(Sites.objects.filter(SiteLink=site_category).first().documents.values_list('LinkDoc', flat=True)))
            except:
                documents_links = []
        documents_links = list(set(documents_links))
    

    documents_query = Documents.objects.filter(LinkDoc__in=documents_links).filter(~Q(Title='missing')).filter(DateDoc__isnull=False).order_by('-DateDoc')

    #query theo từng trang
    index_docs = 0
    if int(page) == 1:
        index_docs = 0
    else:
        index_docs = 2 * (int(page)-1)

    if len(documents_query) <= (index_docs + 1):
        return []

    if documents_query is None:
        documents_query = []
    documents_result = []
    documents_result = [None] * len(documents_query)
    documents_query = [documents_query[index_docs], documents_query[index_docs+1]]
    docs_tmp = []
    for idx, doc in enumerate(documents_query):
        dict_doc = {}
        dict_doc['title'] = striphtml(str(doc.Title).strip())
        dict_doc['tags'] = striphtml(str(doc.Tags).strip(), 'tags')
        dict_doc['author'] = striphtml(str(doc.Author).strip(), 'author')
        site_master_name = Sites_Documents.objects.filter(document=doc.id).first().site.master_name.MasterName
        content_tmp = preprocess(str(doc.Content), site = map_master_site[str(site_master_name)], mode = 'display-user')
        if content_tmp is not None:
            dict_doc['content'] = [{'paragraph' : cont} for cont in content_tmp]
        else:
            dict_doc['content'] = ['NO CONTENT']
        dict_doc['date'] = doc.DateDoc 
        dict_doc['ref'] = str(doc.LinkDoc).strip()
        docs_tmp.append(dict_doc)

    documents_result[index_docs] = docs_tmp[0]
    documents_result[index_docs+1] = docs_tmp[1]
    return documents_result


"""
https://stackoverflow.com/questions/41495052/scrapy-reactor-not-restartable/43661172

from news_site.models import Documents,Sites,ReadyCrawledDocument
sites = Sites.objects.filter(master_name__MasterName='vovworld.vn Lao')

d = ReadyCrawledDocument.objects.all().delete()
exit()

https://stackoverflow.com/questions/56913676/dynamic-updates-in-real-time-to-a-django-template
"""
