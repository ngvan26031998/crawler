
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponse
from django.template import loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from news_site.models import Categories, Languages, Sites, Documents, SiteAttributes, DownloadWarning, ReadyCrawledDocument, SiteMaster, Sites_Documents
from .utils import  scheduler, load_documents
#outer_crawl, set_schedule,
from threading import Thread
from multiprocessing import Process, Queue
import json
import datetime
import asyncio


from .file_rw import write_state, read_state, add_stop, remove_stop, check_stop, get_state
from .preprocess import striphtml, preprocess, process_noise, MarkSection, display_document
from sty import fg, bg, ef, rs, RgbFg
#fg.set_style('purple', RgbFg(240, 29, 233))
#fg.set_style('cyan', RgbFg(40, 235, 228))

def clear_documents(request):
    site_id = request.GET['site_id']
    
    documents_by_site = Sites.objects.filter(id=site_id).first().documents.all()

    site = Sites.objects.filter(id=site_id).first()
 
    docs_id = []
    docs_no_site = []
    for doc in documents_by_site:
        docs_id.append(doc.id)

    Sites_Documents.objects.filter(site=site).delete()
    for doc_id in docs_id:
        if Sites_Documents.objects.filter(document=Documents.objects.filter(id=doc_id).first()).first() is None:
            docs_no_site.append(docs_id)
            Documents.objects.filter(id=doc_id).delete()
    
    response_data = {}
    response_data['result'] = 'Success'
    response_data['site_id'] = site_id
    response_data['sobaixoa'] = len(docs_id)
    return HttpResponse(json.dumps(response_data), content_type='application/json')

def select_site_master(request):
    site_master_id = request.GET['site_master_id']
    response_data = {}
    tongsobai = 0
    sites_query = None
    if str(site_master_id) == '-1':
        sites_query = Sites.objects.all().order_by('SiteName')
    else:        
        sites_query = Sites.objects.filter(master_name__id=site_master_id).order_by('SiteName')
    sites = []
    numb_sites = 0
    for site in sites_query:
        site_json = {}
        site_json['id'] =  site.id
        site_json['name'] = str(site.master_name.MasterName + ' --> ' + site.SiteName)
        site_json['last_succ_count'] = site.last_succ_count
        site_json['total_crawling'] = site.documents.count()
        site_json['schedule_hour'] = site.schedule_hour
        site_json['state'] = str(site.State)
        numb_sites += 1
        tongsobai += site.documents.count()
        sites.append(site_json)

    response_data['numb_sites'] = numb_sites
    response_data['result'] = sites
    response_data['tongsobai'] = tongsobai
    return HttpResponse(json.dumps(response_data), content_type='application/json')


def show_data(request):
    page_number = request.GET.get("page", 1)
    documents_all = load_documents(page=page_number)
    paginator = Paginator(documents_all, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        # Nếu page_number không thuộc kiểu integer, trả về page đầu tiên
        news = paginator.page(1)
    except EmptyPage:
        # Nếu page không có item nào, trả về page cuối cùng
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)

def show_politic(request):
    page_number = request.GET.get("page", 1)
    documents_politic = load_documents(category="politic", page=page_number)
    paginator = Paginator(documents_politic, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)

def show_sports(request):
    page_number = request.GET.get("page", 1)
    documents_sports = load_documents(category="sports", page=page_number)
    paginator = Paginator(documents_sports, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)

def show_business(request):
    page_number = request.GET.get("page", 1)
    documents_business = load_documents(category="business", page=page_number)
    paginator = Paginator(documents_business, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)

def show_world(request):
    page_number = request.GET.get("page", 1)
    documents_world = load_documents(category="world", page=page_number)
    paginator = Paginator(documents_world, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)

def show_technology(request):
    page_number = request.GET.get("page", 1)
    documents_technology = load_documents(category="technology", page=page_number)
    paginator = Paginator(documents_technology, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)

def show_science(request):
    page_number = request.GET.get("page", 1)
    documents_science = load_documents(category="science", page=page_number)
    paginator = Paginator(documents_science, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)

def show_health(request):
    page_number = request.GET.get("page", 1)
    documents_health = load_documents(category="health", page=page_number)
    paginator = Paginator(documents_health, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)

def show_life(request):
    page_number = request.GET.get("page", 1)
    documents_life = load_documents(category="life", page=page_number)
    paginator = Paginator(documents_life, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)


def show_opinion(request):
    page_number = request.GET.get("page", 1)
    documents_opinion = load_documents(category="opinion", page=page_number)
    paginator = Paginator(documents_opinion, 2)
    try:
        news = paginator.page(page_number)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context = {'news' : news}
    return render(request, 'news_site/show_data.html', context)



def update_realime_site(request):
    site_master_id = request.GET['site_master_id']
    response_data = {}
    sites_query = None
    if str(site_master_id) == '-1':
        sites_query = Sites.objects.all().order_by('SiteName')
    else:        
        sites_query = Sites.objects.filter(master_name__id=site_master_id).order_by('SiteName')
    
    sites = []
    tongsobai = 0
    numb_sites = 0  
    for site in sites_query:
        site_json = {}
        site_json['id'] =  site.id
        site_json['last_succ_count'] = site.last_succ_count
        site_json['total_crawling'] = site.documents.count()
        site_json['state'] = str(site.State)
        numb_sites += 1
        tongsobai += site.documents.count()
        sites.append(site_json)
    response_data['result'] = sites
    response_data['tongsobai'] = tongsobai
    return HttpResponse(json.dumps(response_data), content_type='application/json')

def manage_crawler(request):
    site_masters = None#
    dic_job = {}
    try:
        sites_query = Sites.objects.all().order_by('SiteName')
        sites = []
        site_masters_query = SiteMaster.objects.all()
        site_masters = []
        tongsobai = 0
        numb_sites = 0
        for site_master in site_masters_query:
            site_master_json = {}
            site_master_json['id'] = str(site_master.id)
            site_master_json['MasterName'] = str(site_master.MasterName)
            site_masters.append(site_master_json)
        for site in sites_query:
            sites_json = {}
            sites_json['id'] =  site.id
            sites_json['name'] =  str(site.master_name.MasterName + ' --> ' + site.SiteName)
            sites_json['last_succ_count'] = site.last_succ_count
            sites_json['total_crawling'] = site.documents.count()
            sites_json['schedule_hour'] = site.schedule_hour
            sites_json['state'] = str(site.State)
            numb_sites += 1
            sites.append(sites_json)
            tongsobai += site.documents.count()
        for job in scheduler.get_jobs():
            dic_job[int(job.id)] = job.next_run_time
    except Exception as e:
        print(e)
    context = {
        'sites': sites,
        'site_masters': site_masters,
        'dic_job': dic_job,
        'tongsobai':tongsobai,
        'numb_sites' : numb_sites
    }
    #print(site_masters)
    return render(request, 'news_site/manage_crawling_site.html', context)
